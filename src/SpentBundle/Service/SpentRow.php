<?php
namespace SpentBundle\Service;

use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use SpentBundle\Entity\Spent;

class SpentRow
{
    private $router;
    private $checker;

    public function __construct(RouterInterface $router, AuthorizationChecker $checker)
    {
        $this->router = $router;
        $this->checker = $checker;
    }

    public function row(Spent $spent)
    {
        $html = '<tr data-id="'.$spent->getId().'">';
        $html .= '<td class="spent-family">'.(($spent->getFamily()) ? 'Tournemaine' : 'Crenn').'</td>';

        $more = (strlen($spent->getLabel()) > 30) ? '</p><p><a href="#" class="spent-label-more">Afficher plus</a>':"";
        $html .= '<td class="spent-label"><p class="spent-label-p">'.$spent->getLabel().$more.'</p></td>';

        $html .= '<td class="spent-price">'.$spent->getPrice().' €</td>';
        $html .= '<td class="spent-date">'.$spent->getSpentAt()->format('d/m/Y').'</td>';

        $canEdit = $this->checker->isGranted('edit', $spent);
        $canDelete = $this->checker->isGranted('delete', $spent);
        $html .= '<td>';
        if($canEdit || $canDelete){
            if ($canEdit){
                $html .= '<a data-id="'.$spent->getId().'" href="'.$this->router->generate('spent_edit', ['id'=>$spent->getId()]).'" class="btn btn-xs btn-warning spent-edit">Modifier</a>';
            }
            if ($canDelete) {
                $html .= ' <a href="'.$this->router->generate('spent_delete', ['id'=>$spent->getId()]).'" class="btn btn-xs btn-danger spent-delete">Supprimer</a>';
            }
        } elseif (!$spent->isLocked()) {
            if ($spent->getDisagreement() === null){
                $html .= '<a href="'.$this->router->generate('spent_disagree', ['id'=>$spent->getId()]).'" class="btn btn-xs btn-default">Pas d\'accord !</a>';
            } else {
                $html .= '<a href="'.$this->router->generate('spent_agree', ['id'=>$spent->getId()]).'" class="btn btn-xs btn-success">D\'accord !</a>';
            }
        }
        $html .= '</td></tr>';

        return $html;
    }
}