<?php
namespace SpentBundle\Twig;

use SpentBundle\Entity\Spent;
use SpentBundle\Service\SpentRow;

class SpentExtension extends \Twig_Extension
{
    private $row;

    public function __construct(SpentRow $row)
    {
        $this->row = $row;
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('spent_row', array($this, 'rowFilter')),
        );
    }

    public function rowFilter(Spent $spent)
    {
        return $this->row->row($spent);
    }

    public function getName()
    {
        return 'spent_extension';
    }
}