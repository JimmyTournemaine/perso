<?php
namespace SpentBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use SpentBundle\Entity\Subscription;
use SpentBundle\Entity\Spent;

class SubscriptionCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('spent:subscription:generate')
            ->setDescription('Create a new spent from subscription')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $subs = $em->getRepository("SpentBundle:Subscription")->findAll();

        $now = new \DateTime();
        $today = $now->format('d');
        $count = 0;

        $output->writeln('Spent generating from subscriptions : '.$now->format("d/m/Y"));
        $output->writeln('==================================================');

        foreach ($subs as $sub)
        {
            if ($sub->getDebitedAt() == $today)
            {
                $spent = new Spent();
                $spent->setFamily($sub->getFamily());
                $spent->setLabel($sub->getLabel());
                $spent->setOwner($sub->getOwner());
                $spent->setPrice($sub->getPrice());
                $spent->setSpentAt($now);
                $em->persist($spent);

                $output->writeln(++$count . " - $spent has been generated.");
            }
        }
        $em->flush();
        $output->writeln("Generated spents : $count");
    }
}