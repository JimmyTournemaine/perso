<?php
namespace SpentBundle\Security;

use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use SpentBundle\Entity\Spent;

class SpentVoter extends Voter
{
    const EDIT = 'edit';
    const DELETE = 'delete';

    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, array(self::EDIT, self::DELETE)))
            return false;

        if(!$subject instanceof Spent)
            return false;

        return true;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        if(!$user instanceof UserInterface){
            return false;
        }

        return $this->canEditAndDelete($subject, $user);
    }

    private function canEditAndDelete(Spent $spent, UserInterface $user)
    {
        return $spent->getOwner() == $user && !$spent->isLocked();
    }

}