<?php

namespace SpentBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SubscriptionType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fromModel', AbstractSpentType::class, array(
                'label' => false,
                'data_class' => 'SpentBundle\Entity\Spent'
            ))
            ->add('debitedAt', IntegerType::class, array(
                'label' => 'Jour du débit',
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SpentBundle\Entity\Subscription'
        ));
    }
}
