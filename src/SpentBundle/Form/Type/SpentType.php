<?php

namespace SpentBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SpentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $year = (int) (new \DateTime())->format('Y');

        $builder
            ->add('fromModel', AbstractSpentType::class, array(
                'label' => 'Détails',
                'data_class' => 'SpentBundle\Entity\Spent'
            ))
            ->add('spentAt', DateType::class, array(
                'label' => 'Date',
                'data' => new \DateTime(),
                'years' => array($year-1, $year),
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SpentBundle\Entity\Spent'
        ));
    }
}
