<?php
namespace SpentBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use SpentBundle\Entity\Spent;
use SpentBundle\Form\Type\SpentType;
use SpentBundle\Entity\Settlement;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use AppBundle\Service\Flashify;

/**
 * Spent controller.
 */
class SpentController extends Controller
{
    /**
     * Lists all Spent entities.
     *
     * @Route("/", name="spent_index")
     * @Method({"GET"})
     */
    public function indexAction(Request $request)
    {
        $repo = $this->getDoctrine()->getManager()->getRepository('SpentBundle:Spent');

        /* Get current spents */
        $spents = $repo->findNotSettled();

        /* Compute totals */
        $totals = array();
        foreach($repo->totalByFamily() as $total){
            $totals[boolval($total['family'])] = $total['total'];
        }
        if (sizeof($totals) < 2){
            if(!isset($totals[true])){
                $totals[true] = 0;
            }
            if(!isset($totals[false])){
                $totals[false] = 0;
            }
        }

        /* New spent form */
        $spent = new Spent();
        $form = $this->createForm(SpentType::class, $spent, array('action' => $this->generateUrl('spent_new')));

        /* Settlement form */
        $settlement = new Settlement();
        $settlementForm = $this->createSettlementForm($settlement);

        return $this->render('SpentBundle:Spent:index.html.twig', array(
            'form' => $form->createView(),
            'settleForm' => $settlementForm->createView(),
            'spents' => $spents,
            'totals' => $totals,
        ));
    }

    /**
     * Creates a new Spent entity.
     *
     * @Route("/new", name="spent_new")
     * @Method("POST")
     */
    public function newAction(Request $request)
    {
        $spent = new Spent();
        $form = $this->createForm(SpentType::class, $spent);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $spent->setOwner($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($spent);
            $em->flush();
            $this->addFlash('success', 'Nouvel dépense enregistrée !');
            return $this->redirectToRoute('spent_index');
        }

        return $this->render('SpentBundle:Spent:new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Spent entity.
     *
     * @Route("/{id}/edit", name="spent_edit", options={"expose": true})
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Spent $spent)
    {
        $this->denyAccessUnlessGranted('edit', $spent);

        if($request->isXmlHttpRequest()){
            return $this->ajaxEditAction($request, $spent);
        }

        $editForm = $this->createForm(SpentType::class, $spent);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($spent);
            $em->flush();

            return $this->redirectToRoute('spent_index');
        }

        return $this->render('SpentBundle:Spent:edit.html.twig', array(
            'spent' => $spent,
            'edit_form' => $editForm->createView()
        ));
    }

    protected function ajaxEditAction(Request $request, Spent $spent)
    {
        $data = $request->request->get('spent');
        $spent->setLabel($data['label']);
        $spent->setPrice($data['price']);
        $spent->setSpentAt(\DateTime::createFromFormat('d/m/Y h:i', $data['spentAt'].' 00:00'));

        $validator = $this->get('validator');
        $errors = $validator->validate($spent);
        if (count($errors) > 0) {
            $flashify = $this->get('app.flashify');
            $error = "";
            foreach ($errors as $violation){
                $error .= $flashify->toFlash($flashify::TYPE_ERROR, $violation->getMessage());
            }
            return new Response($error, Response::HTTP_NOT_ACCEPTABLE);
        }


        $em = $this->getDoctrine()->getManager();
        $em->persist($spent);
        $em->flush();
        $em->refresh($spent);

        return new Response($this->get('spent.row')->row($spent));
    }

    /**
     * Deletes a Spent entity.
     *
     * @Route("/{id}/delete", name="spent_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, Spent $spent)
    {
        $this->denyAccessUnlessGranted('delete', $spent);

        $em = $this->getDoctrine()->getManager();
        $em->remove($spent);
        $em->flush();

        if ($request->isXmlHttpRequest()){
            return new Response();
        }
        return $this->redirectToRoute('spent_index');
    }

    /**
     * Disagreement
     *
     * @Route("/{id}/disagree", name="spent_disagree")
     * @Method({"GET","POST"})
     */
    public function disagreeAction(Request $request, Spent $spent)
    {
        $form = $this->createFormBuilder($spent)
            ->add('disagreement', null, array('label' => 'Commentaire'))
            ->getForm()
            ->handleRequest($request);
        ;

        if ($form->isSubmitted() && $form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($spent);
            $em->flush();

            return $this->redirectToRoute('spent_index');
        }

        return $this->render('SpentBundle:Spent:disagree.html.twig', array(
            'spent' => $spent,
            'form' => $form->createView(),
        ));
    }

    /**
     * Agreement
     *
     * @Route("/{id}/agree", name="spent_agree")
     * @Method({"GET"})
     */
    public function agreeAction(Spent $spent)
    {
        $spent->setDisagreement(null);

        $em = $this->getDoctrine()->getManager();
        $em->persist($spent);
        $em->flush();

        return $this->redirectToRoute('spent_index');
    }

    /**
     * @Route("/settlement", name="spent_settlement")
     * @Method("POST")
     */
    public function settlementAction(Request $request)
    {
        $settlement = new Settlement();
        $form = $this->createSettlementForm($settlement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            foreach ($em->getRepository("SpentBundle:Spent")->findNotSettledBefore($settlement->getSettledAt()) as $spent) {
                $settlement->addSpent($spent);
            }

            if ($settlement->getSpents()->count() > 0) {
                $em->persist($settlement);
                $em->flush();

                return $this->redirectToRoute('spent_index');
            } else {
                $this->addFlash(Flashify::TYPE_ERROR, 'Impossible de procéder au réglément, aucune dépense n\'a eu lieu avant la date fournie.');
            }
        }
        return $this->redirectToRoute('spent_index');
    }

    private function createSettlementForm($settlement = null)
    {
        return $this->createFormBuilder(($settlement) ? $settlement : new Settlement(), array('action' => $this->generateUrl('spent_settlement')))
            ->add('settledAt', DateType::class, array('label' => 'Tout est réglé jusqu\'au : ', 'data' => new \DateTime()))
            ->getForm()
        ;
    }

    /**
     * @Route("/archives", name="spent_settlement_archives")
     */
    public function settlementsArchivesAction()
    {
        $settlements = $this->getDoctrine()->getManager()->getRepository("SpentBundle:Settlement")->findOldSettlements();

        return $this->render('SpentBundle:Settlement:archive.html.twig', array(
            'settlements' => $settlements,
        ));

    }
}
