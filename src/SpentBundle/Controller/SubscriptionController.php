<?php

namespace SpentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use SpentBundle\Entity\Subscription;
use SpentBundle\Form\Type\SubscriptionType;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/subscriptions")
 */
class SubscriptionController extends Controller
{
    /**
     * @Route("/", name="spent_subscription_index")
     */
    public function indexAction()
    {
        $subscriptions = $this->getDoctrine()->getManager()->getRepository("SpentBundle:Subscription")->findAll();

        return $this->render('SpentBundle:Subscription:index.html.twig', array(
            'subscriptions' => $subscriptions,
        ));
    }

    /**
     * @Route("/new", name="spent_subscription_new")
     */
    public function newAction(Request $request)
    {
        $sub = new Subscription();
        $form = $this->createForm(SubscriptionType::class, $sub);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $sub->setOwner($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($sub);
            $em->flush();

            return $this->redirectToRoute('spent_subscription_index');
        }

        return $this->render('SpentBundle:Subscription:new.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/{id}/edit", name="spent_subscription_edit")
     */
    public function editAction(Request $request, Subscription $sub)
    {
        $this->denyAccessUnlessGranted('edit', $sub);

        $form = $this->createForm(SubscriptionType::class, $sub);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $sub->setOwner($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($sub);
            $em->flush();

            return $this->redirectToRoute('spent_subscription_index');
        }

        return $this->render('SpentBundle:Subscription:edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/{id}/delete", name="spent_subscription_delete")
     */
    public function deleteAction(Request $request, Subscription $sub)
    {
        $this->denyAccessUnlessGranted('delete', $sub);
        $form = $this->createFormBuilder()->getForm()->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->remove($sub);
            $em->flush();

            return $this->redirectToRoute('spent_subscription_index');
        }

        return $this->render('SpentBundle:Subscription:delete.html.twig', array(
            'form' => $form->createView()
        ));
    }

}
