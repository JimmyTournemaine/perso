<?php

namespace SpentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Settlement
 *
 * @ORM\Table(name="settlement")
 * @ORM\Entity(repositoryClass="SpentBundle\Repository\SettlementRepository")
 */
class Settlement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="settled_at", type="datetime")
     */
    private $settledAt;

    /**
     * @var string
     *
     * @ORM\OneToMany(targetEntity="SpentBundle\Entity\Spent", mappedBy="settlement")
     */
    private $spents;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->spents = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set settledAt
     *
     * @param \DateTime $settledAt
     *
     * @return Settlement
     */
    public function setSettledAt(\DateTime $settledAt)
    {
        $this->settledAt = $settledAt;

        return $this;
    }

    /**
     * Get settledAt
     *
     * @return \DateTime
     */
    public function getSettledAt()
    {
        return $this->settledAt;
    }

    /**
     * Add spent
     *
     * @param \SpentBundle\Entity\Spent $spent
     *
     * @return Settlement
     */
    public function addSpent(\SpentBundle\Entity\Spent $spent)
    {
        $spent->setSettlement($this);
        $this->spents[] = $spent;

        return $this;
    }

    /**
     * Remove spent
     *
     * @param \SpentBundle\Entity\Spent $spent
     */
    public function removeSpent(\SpentBundle\Entity\Spent $spent)
    {
        $this->spents->removeElement($spent);
    }

    /**
     * Get spents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSpents()
    {
        return $this->spents;
    }
}
