<?php

namespace SpentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use SpentBundle\Model\Spent as BaseSpent;

/**
 * Subscription
 *
 * @ORM\Table(name="subscription")
 * @ORM\Entity(repositoryClass="SpentBundle\Repository\SubscriptionRepository")
 */
class Subscription extends BaseSpent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="debited_at", type="integer")
     * @Assert\NotBlank()
     * @Assert\Range(min=1,max=28)
     */
    private $debitedAt;

    public function getDebitedAt()
    {
        return $this->debitedAt;
    }

    public function setDebitedAt($debitedAt)
    {
        $this->debitedAt = $debitedAt;
        return $this;
    }
}

