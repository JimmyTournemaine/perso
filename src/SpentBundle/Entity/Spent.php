<?php

namespace SpentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use SpentBundle\Model\Spent as BaseSpent;

/**
 * Spent
 *
 * @ORM\Table(name="spent")
 * @ORM\Entity(repositoryClass="SpentBundle\Repository\SpentRepository")
 */
class Spent extends BaseSpent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="spentAt", type="date")
     * @Assert\Date(message="La date n'est pas valide !")
     * @Assert\LessThanOrEqual("today", message="Tu peux dépenser dans le futur ? =O")
     */
    private $spentAt;

    /**
     * @ORM\Column(name="disagreement", type="text", nullable=true)
     */
    private $disagreement;

    /**
     * @ORM\ManyToOne(targetEntity="SpentBundle\Entity\Settlement", inversedBy="spents")
     * @ORM\JoinColumn(nullable=true)
     */
    private $settlement;

    public function __toString()
    {
        return $this->label;
    }

    public function isLocked()
    {
        return $this->settlement !== null;
    }

    public function setSpentAt($spentAt)
    {
        $this->spentAt = $spentAt;

        return $this;
    }

    public function getSpentAt()
    {
        return $this->spentAt;
    }

    public function getDisagreement()
    {
        return $this->disagreement;
    }

    public function setDisagreement($disagreement)
    {
        $this->disagreement = $disagreement;
        return $this;
    }

    public function getSettlement()
    {
        return $this->settlement;
    }

    public function setSettlement(\SpentBundle\Entity\Settlement $settlement)
    {
        $this->settlement = $settlement;
        return $this;
    }



}
