<?php

namespace SpentBundle\Repository;

use Doctrine\ORM\Query\Expr\Join;

/**
 * SettlementRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SettlementRepository extends \Doctrine\ORM\EntityRepository
{
    public function findOldSettlements()
    {
        return $this->_em->createQueryBuilder()
            ->select('s')
            ->from('SpentBundle:Spent', 'sp')
            ->innerJoin('SpentBundle:Settlement', 's', Join::WITH, 's = sp.settlement')
            ->orderBy('s.settledAt', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }
}
