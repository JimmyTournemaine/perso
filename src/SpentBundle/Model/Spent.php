<?php
namespace SpentBundle\Model;

use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\MappedSuperclass
 */
abstract class Spent
{
    protected $id;

    /**
     * @ORM\Column(name="family", type="boolean")
     */
    protected $family;

    /**
     * @ORM\Column(name="price", type="decimal", precision=5, scale=2)
     * @Assert\NotBlank(message="Combien as-tu dépensé ?")
     * @Assert\Range(min=0, max=199.99, minMessage="Tu es sûre du prix ?", maxMessage="Beaucoup trop cher !")
     */
    protected $price;

    /**
     * @ORM\Column(name="label", type="string", length=255)
     * @Assert\NotBlank(message="Qu'as-tu payé ?")
     * @Assert\Length(min=3, minMessage="Tu peux être plus précis à propos de cette dépense ?", max=255, maxMessage="La base de donnée bloque le nombre de caractères autorisés à 255 !")
     */
    protected $label;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     */
    protected $owner;

    public function getId()
    {
        return $this->id;
    }

    public function getFamily()
    {
        return $this->family;
    }

    public function setFamily($family)
    {
        $this->family = $family;
        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    public function getOwner()
    {
        return $this->owner;
    }

    public function setOwner(UserInterface $owner)
    {
        $this->owner = $owner;
        return $this;
    }

}