<?php
namespace JT\BlogBundle\Validation\Constraint;

use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;

class ArticlePasswordConstraintValidator extends ConstraintValidator
{
    private $encoder;

    public function __construct($encoderClass){
        $this->encoder = new $encoderClass;
    }

    public function validate($value, Constraint $constraint)
    {
        if (!$this->encoder->isPasswordValid($constraint->article->getPassword(), $constraint->article, $value)){
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}