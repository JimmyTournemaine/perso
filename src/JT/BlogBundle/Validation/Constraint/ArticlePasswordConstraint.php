<?php
namespace JT\BlogBundle\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

class ArticlePasswordConstraint extends Constraint
{
    public $message = 'jt_blog.article.password.invalid';
    public $article;

    public function __construct($article, $options=null)
    {
        $this->article = $article;
        parent::__construct($options);
    }

    public function validatedBy()
    {
        return 'jt_blog_article_password_validator';
    }
}