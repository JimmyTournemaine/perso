<?php
namespace JT\BlogBundle\Form\Listener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class PasswordSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_SET_DATA => 'addPasswordField',
        );
    }

    public function addPasswordField(FormEvent $event)
    {
        $event->getForm()->add('password', RepeatedType::class, array(
            'first_options' => array('label' => 'form.label.article.password.first'),
            'second_options' => array('label' => 'form.label.article.password.second'),
            'type' => PasswordType::class,
        ));
    }
}