<?php
namespace JT\BlogBundle\Form\Listener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class CategorySubscriber implements EventSubscriberInterface
{
    private $categoryClass;

    public function __construct($categoryClass)
    {
        $this->categoryClass = $categoryClass;
    }

    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_SET_DATA => 'addCategoryField',
        );
    }

    public function addCategoryField(FormEvent $event)
    {
        $event->getForm()
            ->add('category', EntityType::class, array(
                'label' => 'form.label.article.category',
                'class' => $this->categoryClass,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('c')->orderBy('c.name');
                }
            ))
        ;
    }
}