<?php
namespace JT\BlogBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class ArrayToStringTransformer implements DataTransformerInterface
{
    public function transform($array)
    {
        if (sizeof($array) < 1){
            return '';
        }

        return implode(',', $array);
    }

    public function reverseTransform($string)
    {
        if (!$string){
            return array();
        }

        return explode(',', $string);
    }
}