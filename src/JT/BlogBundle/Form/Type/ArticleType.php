<?php

namespace JT\BlogBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use JT\BlogBundle\Form\Listener\PasswordSubscriber;
use JT\BlogBundle\Form\Listener\CategorySubscriber;
use JT\BlogBundle\Form\DataTransformer\ArrayToStringTransformer;

class ArticleType extends AbstractType
{
    private $articleClass;
    private $byPassword;
    private $category;

    public function __construct($article_class, $by_password, $category)
    {
        $this->articleClass = $article_class;
        $this->byPassword = $by_password;
        $this->category = $category;
    }

    public function setCategoryClass($class)
    {
        $this->categoryClass = $class;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array(
                'label' => 'form.label.article.title'
            ))
            ->add('content', CKEditorType::class, array(
                'label' => 'form.label.article.content'
            ))
            ->add('tags', TextType::class, array(
                'label' => 'form.label.article.tags'
            ))
            ->add('published', CheckboxType::class, array(
                'label' => 'form.label.article.published'
            ))
        ;

        $builder->get('tags')->addModelTransformer(new ArrayToStringTransformer());

        if ($this->category) {
            $builder->addEventSubscriber(new CategorySubscriber($this->categoryClass));
        }

        if ($this->byPassword){
            $builder->addEventSubscriber(new PasswordSubscriber());
        }
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->articleClass,
            'translation_domain' => 'JTBlogBundle'
        ));
    }
}

