<?php
namespace JT\BlogBundle\SpamDetection;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use FOS\CommentBundle\Model\CommentInterface;

class SpamDetector implements SpamDetectorInterface
{
    private $stack;
    private $manager;

    public function __construct(EntityManagerInterface $entityManager, RequestStack $stack)
    {
        $this->stack = $stack;
        $this->manager = $entityManager;
    }

    public function isSpam()
    {
        if(null !== $this->manager->getRepository('JTBlogBundle:BlackList')->find($this->stack->getMasterRequest()->getClientIp())){
            return true;
        }

        return false;
    }

    public function register(CommentInterface $comment)
    {
        if($this->isSpam() === true) return;

	    $this->manager->persist($comment);
	    $this->manager->flush();
    }
}
