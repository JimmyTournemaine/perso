<?php
namespace JT\BlogBundle\SpamDetection;

use FOS\CommentBundle\Model\CommentInterface;

interface SpamDetectorInterface
{
    public function isSpam();
    public function register(CommentInterface $comment);
}