<?php
namespace JT\BlogBundle\Entity;

use FOS\CommentBundle\Entity\Thread as BaseThread;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Jimmy Tournemaine <jimmy.tournemaine@yahoo.fr>
 * @ORM\MappedSuperclass
 */
class Thread extends BaseThread
{
}