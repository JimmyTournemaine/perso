<?php
namespace JT\BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @author Jimmy Tournemaine <jimmy.tournemaine@yahoo.fr>
 * @ORM\Entity
 * @ORM\Table(name="blog_banned_word")
 * @UniqueEntity("word")
 */
class BannedWord
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="word", type="string", length=31, unique=true)
     * @Assert\NotBlank()
     * @Assert\Length(min=3,max=255)
     */
    private $word;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function getWord()
    {
        return $this->word;
    }

    public function setWord($word)
    {
        $this->word = $word;
    }
}
