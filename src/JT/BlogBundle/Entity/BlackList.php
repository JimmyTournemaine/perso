<?php

namespace JT\BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * BlackList
 *
 * @ORM\Table(name="black_list")
 * @ORM\Entity(repositoryClass="BlogBundle\Repository\BlackListRepository")
 */
class BlackList
{
    /**
     * @var array
     * @ORM\Id
     * @ORM\Column(name="ip", type="string", length=39, nullable=false)
     * @Assert\Ip
     */
    private $ip;

    public function getIp()
    {
        return $this->ip;
    }

    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }
}

