<?php
namespace JT\BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\CommentBundle\Entity\Comment as BaseComment;
use Symfony\Component\Security\Core\User\UserInterface;
use JT\BlogBundle\Model\SignedCommentInterface;

/**
 * @ORM\MappedSuperclass
 * @author Jimmy Tournemaine <jimmy.tournemaine@yahoo.fr>
 */
class Comment extends BaseComment implements SignedCommentInterface
{

    /**
     * @ORM\ManyToOne(targetEntity="JT\BlogBundle\Model\AuthorInterface")
     * @ORM\JoinColumn(name="author")
     */
    protected $author;

    public function setAuthor(UserInterface $author)
    {
        $this->author = $author;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function getAuthorName()
    {
        if (null === $this->getAuthor()){
            return 'Anonymous';
        }

        if (null === $name = $this->getAuthor()->getAuthorName()){
            throw new \LogicException(get_class($this->getAuthor()).'::getAuthorName() has to return a non null value.');
        }

        return $name;
    }
}