<?php

namespace JT\BlogBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Reference;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class JTBlogExtension extends Extension implements PrependExtensionInterface
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services/services.yml');

        /* Process Article Configuration */
        $article = $config['article'];
        $container->setParameter('jt_blog.article.class', $article['class']);
        $container->setParameter('jt_blog.article.per_page', $article['per_page']);
        $container->setParameter('jt_blog.article.form', $article['form']);
        $container->setParameter('jt_blog.article.owner', $article['owner']);
        $container->setParameter('jt_blog.article.master', $article['master']);
        // By Password
        $bp = $article['by_password'];
        $container->setParameter('jt_blog.article.by_password.enabled', $bp['enabled']);
        if ($bp['enabled']){
            $loader->load('services/by_password.yml');
            $container->setParameter('jt_blog.article.by_password.encoder', $bp['encoder']);
        }

        /* Process Category Configuration */
        $category = $config['category'];
        $container->setParameter('jt_blog.category.enabled', $category['enabled']);
        if ($category['enabled']){
            $loader->load('services/category.yml');
            $container->setParameter('jt_blog.category.class', $category['class']);
            $container->setParameter('jt_blog.category.form', $category['form']);
            $container->setParameter('jt_blog.category.role', $category['role']);
        }

        /* Process non-FOS comment config */
        $comment = $config['comment'];
        if (isset($comment['censure'])) {
            $censure = $comment['censure'];
            $loader->load('services/censure.yml');
            $container->setParameter('jt_blog.comment.censure.role', $censure['banned_word_access']);
            $container->getDefinition('jt_blog.censure_listener')->addArgument(new Reference($censure['censor']));
        }

        if (isset($comment['spam_detection'])) {
            $spam = $comment['spam_detection'];
            $loader->load('services/spam.yml');
            $container->getDefinition('jt_blog.spam_subscriber')->addArgument(new Reference($spam['service']));
        }
    }

    public function prepend(ContainerBuilder $container)
    {
        /*$bundle = $container->getParameter('kernel.bundles');
        if (!isset($bundles['FOSCommentBundle'])){
            return;
        }*/

        $configs = $container->getExtensionConfig($this->getAlias());
        $config = $this->processConfiguration(new Configuration(), $configs);
        if (isset($config['comment'])){
            $comment = $config['comment'];
            $fosconfig = array(
                'db_driver' => $comment['db_driver'],
                'class' => $comment['class'],
            );
            if (isset($comment['form'])){
                $fosconfig['form'] = $comment['form'];
            }
            if (isset($comment['service'])){
                $fosconfig['service'] = $comment['service'];
            }
            $container->prependExtensionConfig('fos_comment', $fosconfig);
        }
    }
}
