<?php

namespace JT\BlogBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('jt_blog');

        $rootNode
        ->addDefaultsIfNotSet()
            ->children()
                ->arrayNode('article')
                    ->children()
                        ->scalarNode('class')->isRequired()->end()
                        ->integerNode('per_page')->defaultValue(5)->end()
                        ->scalarNode('owner')
                            ->defaultValue('ROLE_SUPER_ADMIN')
                            ->info('Role needed to write/update/delete your own articles')
                        ->end()
                        ->scalarNode('master')
                            ->defaultValue('ROLE_SUPER_ADMIN')
                            ->info('Role needed to write/update/delete all articles')
                        ->end()
                        ->scalarNode('form')
                            ->defaultValue('JT\BlogBundle\Form\Type\ArticleType')
                        ->end()
                        ->arrayNode('by_password')
                            ->children()
                                ->booleanNode('enabled')->defaultFalse()->end()
                                ->scalarNode('encoder')->defaultValue('JT\BlogBundle\Encoder\Sha256Salted')->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('category')
                    ->children()
                        ->booleanNode('enabled')->defaultFalse()->end()
                        ->scalarNode('class')->defaultNull()->end()
                        ->scalarNode('form')->defaultValue('JT\BlogBundle\Form\Type\CategoryType')->end()
                        ->scalarNode('role')->defaultValue('ROLE_SUPER_ADMIN')->end()
                    ->end()
                ->end()
                ->arrayNode('comment')
                    ->children()
                        ->scalarNode('db_driver')->isRequired()->end()
                        ->arrayNode('class')->isRequired()
                            ->children()
                                ->arrayNode('model')->isRequired()
                                    ->children()
                                        ->scalarNode('comment')->isRequired()->end()
                                        ->scalarNode('thread')->isRequired()->end()
                                        ->scalarNode('voter')->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                        ->arrayNode('form')
                            ->prototype('array')
                                ->children()
                                    ->scalarNode('type')->isRequired()->end()
                                    ->scalarNode('name')->end()
                                ->end()
                            ->end()
                        ->end()
                        ->arrayNode('censure')
                            ->children()
                                ->scalarNode('banned_word_access')->isRequired()->end()
                                ->scalarNode('censor')->defaultValue('jt_blog.censor')->end()
                            ->end()
                        ->end()
                        ->arrayNode('spam_detection')
                            ->children()
                                ->scalarNode('service')->defaultValue('jt_blog.spam_detector')->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
