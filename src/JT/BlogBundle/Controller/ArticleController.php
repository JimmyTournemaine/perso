<?php

namespace JT\BlogBundle\Controller;

use JT\BlogBundle\Entity\Article;
use JT\BlogBundle\Validation\Constraint\ArticlePasswordConstraint;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Article controller.
 *
 * @Route("/article")
 */
class ArticleController extends Controller
{
    /**
     * Lists all Article entities.
     *
     * @Route("/{page}", requirements={"page" = "\d+"}, defaults={"page"=1}, name="jt_blog.article_index")
     * @Method("GET")
     */
    public function indexAction(Request $request, $page)
    {
        if ($page < 1){
            throw $this->createNotFoundException();
        }

        // Initializations
        $em = $this->getDoctrine()->getManager();
        $articleClass = $this->getParameter('jt_blog.article.class');
        $perPage = $this->getParameter('jt_blog.article.per_page');
        $offset = ($page - 1) * $perPage;

        // Queries
        $count = (int) $em->createQuery("SELECT count(a) FROM $articleClass a WHERE a.published = 1 AND a.publishedAt is null")->getSingleScalarResult();
        $articles = $em->getRepository($articleClass)->findBy(
            array('published' => 1, 'password' => null),
            array('publishedAt' => 'DESC'),
            $perPage,
            $offset);
        if ($articles === null){
            throw $this->createNotFoundException();
        }

        return $this->render('JTBlogBundle:article:index.html.twig', array(
            'page' => $page,
            'articles' => $articles,
            'count' => $count,
            'pages' => ($pages = round($count/$perPage)) > 0 ? $pages : 1,
        ));
    }

    /**
     * Creates a new Article entity.
     *
     * @Route("/new", name="jt_blog.article_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        // Check access
        $this->denyAccessUnlessGranted(array(
            $this->getParameter('jt_blog.article.master'),
            $this->getParameter('jt_blog.article.owner')
        ));

        // Form handling
        $articleClass = $this->getParameter('jt_blog.article.class');
        $article = new $articleClass;
        $form = $this->createForm($this->getParameter('jt_blog.article.form'), $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // Password Encode
            if ($form->has('password') && $article->getPassword()){
                $article->setPassword($this->getParameter('jt_blog.article.by_password.encoder')->encodePassword($article, $article->getPassword()));
            }

            // Author setting
            $article->setAuthor($this->getUser());

            // Record
            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            return $this->redirectToRoute('jt_blog.article_show', array('slug' => $article->getSlug()));
        }

        return $this->render('JTBlogBundle:article:new.html.twig', array(
            'article' => $article,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Article entity.
     *
     * @Route("/{slug}", name="jt_blog.article_show")
     * @Method({"GET","POST"})
     */
    public function showAction(Request $request, $slug)
    {
        // Get the article
        $articleClass = $this->getParameter('jt_blog.article.class');
        $article = $this->getDoctrine()->getManager()->getRepository($articleClass)->findOneBySlug($slug);
        if ($article === null){
            throw $this->createNotFoundException();
        }

        // Check access
        $vipAccess = $article->getAuthor() == $this->getUser() || $this->isGranted($this->getParameter('jt_blog.article.master'));
        if ($article->isPublished() === false && $vipAccess === false) {
            throw $this->createAccessDeniedException();
        }

        // Access if No password, Vip, Password valid
        $form = $this->createPasswordForm($article)->handleRequest($request);
        if ($article->getPassword() === null || $form->isValid() || $vipAccess) {
            return $this->render('JTBlogBundle:article:show.html.twig', array(
                'article' => $article,
            ));
        }

        // Else Password Form
        return $this->render('JTBlogBundle:article:show_private.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * Displays a form to edit an existing Article entity.
     *
     * @Route("/{slug}/edit", name="jt_blog.article_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, $slug)
    {
        // Get the article
        $articleClass = $this->getParameter('jt_blog.article.class');
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository($articleClass)->findOneBySlug($slug);
        if ($article === null){
            throw $this->createNotFoundException();
        }

        // Check access
        if ($article->getAuthor() != $this->getUser() && !$this->isGranted($this->getParameter('jt_blog.article.master'))){
            throw $this->createAccessDeniedException();
        }

        // Handle forms
        $deleteForm = $this->createDeleteForm($article);
        $editForm = $this->createForm($this->getParameter('jt_blog.article.form'), $article);
        $editForm->handleRequest($request);

        // Update entity
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            return $this->redirectToRoute('jt_blog.article_edit', array('slug' => $article->getSlug()));
        }

        // Render template
        return $this->render('JTBlogBundle:article:edit.html.twig', array(
            'article' => $article,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Article entity.
     *
     * @Route("/{id}", name="jt_blog.article_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        // Get Article
        $articleClass = $this->getParameter('jt_blog.article.class');
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository($articleClass)->find($id);
        if ($article === null){
            throw $this->createNotFoundException();
        }

        // Form Handling
        $form = $this->createDeleteForm($article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->remove($article);
            $em->flush();
        }

        return $this->redirectToRoute('jt_blog.article_index');
    }

    private function createPasswordForm(Article $article){
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('jt_blog.article_show', array('slug' => $article->getSlug())))
            ->setMethod('POST')
            ->add('password', PasswordType::class, array(
                'label' => 'form.label.password',
                'constraints' => array(new ArticlePasswordConstraint($article)),
                'translation_domain' => 'JTBlogBundle'
            ))
            ->getForm()
        ;
    }

    /**
     * Creates a form to delete a Article entity.
     *
     * @param Article $article The Article entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Article $article)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('jt_blog.article_delete', array('id' => $article->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
