<?php

namespace JT\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use JT\BlogBundle\Entity\BannedWord;
use JT\BlogBundle\Form\Type\BannedWordType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;


/**
 * @Route("/banned/word")
 * @author Jimmy Tournemaine <jimmy.tournemaine@yahoo.fr>
 */
class BannedWordController extends Controller
{
    /**
     * @Route("/", name="jt_blog.banned_word.index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $this->denyAccessUnlessGranted($this->getParameter('jt_blog.comment.censure.role'));

        $em = $this->getDoctrine()->getManager();
        $words = $em->getRepository("JTBlogBundle:BannedWord")->findBy([], array('word' => 'ASC'));
        $newForm = $this->createBannedWordForm(new BannedWord());

        return $this->render('JTBlogBundle:bannedWord:index.html.twig', array(
            'bannedWords' => $words,
            'newForm' => $newForm->createView()
        ));
    }

    /**
     * @Route("/new", name="jt_blog.banned_word.new")
     * @Method("POST")
     */
    public function newAction(Request $request)
    {
        $this->denyAccessUnlessGranted($this->getParameter('jt_blog.comment.censure.role'));

        $bannedWord = new BannedWord();
        $form = $this->createBannedWordForm($bannedWord);
        $ajax = $request->isXmlHttpRequest();

        $form->handleRequest($request);

        if ($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($bannedWord);
            $em->flush();

            return ($ajax) ? new Response($bannedWord->getWord()) : $this->redirectToRoute('jt_blog.banned_word.index');
        }

        if ($ajax) {
            return new Response(
                $this->renderView('JTBlogBundle:bannedWord:form.html.twig', array('newForm' => $form->createView())),
                Response::HTTP_NOT_ACCEPTABLE
            );
        }

        return $this->render('JTBlogBundle:bannedWord:new.html.twig', array(
            'newForm' => $form->createView(),
        ));
    }

    /**
     * @Route("/delete/{id}", name="jt_blog.banned_word.delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, BannedWord $bannedWord)
    {
        $this->denyAccessUnlessGranted($this->getParameter('jt_blog.comment.censure.role'));

        $em = $this->getDoctrine()->getManager();
        $em->remove($bannedWord);
        $em->flush();

        return ($request->isXmlHttpRequest()) ? new Response() : $this->redirectToRoute('jt_blog.banned_word.index');
    }

    private function createBannedWordForm(BannedWord $bannedWord)
    {
        return $this->createForm(BannedWordType::class, $bannedWord);
    }
}
