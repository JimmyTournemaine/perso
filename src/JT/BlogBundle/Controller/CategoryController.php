<?php

namespace JT\BlogBundle\Controller;

use JT\BlogBundle\Entity\Category;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
/**
 * Category controller.
 *
 * @Route("/category")
 */
class CategoryController extends Controller
{
    /**
     * Lists all Category entities.
     *
     * @Route("/", name="jt_blog.category_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $this->denyAccessUnlessGranted($this->getParameter('jt_blog.category.role'));
        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository($this->getCategoryClass())->findBy(array('parent' => null));

        return $this->render('JTBlogBundle:category:index.html.twig', array(
            'categories' => $categories,
        ));
    }

    /**
     * Creates a new Category entity.
     *
     * @Route("/new", name="jt_blog.category_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $this->denyAccessUnlessGranted($this->getParameter('jt_blog.category.role'));

        $categoryClass = $this->getCategoryClass();
        $category = new $categoryClass;
        $form = $this->createForm($this->getParameter('jt_blog.category.form'), $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('jt_blog.category_index');
        }

        return $this->render('JTBlogBundle:category:new.html.twig', array(
            'category' => $category,
            'form' => $form->createView(),
        ));
    }

    /**
     * Create a new category as a child of an other one.
     *
     * @Route("/{id}/child", name="jt_blog.category_new_child")
     * @Method({"GET","POST"})
     */
    public function newChildAction(Request $request, $id)
    {
        $this->denyAccessUnlessGranted($this->getParameter('jt_blog.category.role'));

        $categoryClass = $this->getCategoryClass();
        $em = $this->getDoctrine()->getManager();
        $parent = $em->getRepository($categoryClass)->find($id);
        if ($parent === null){
            throw $this->createNotFoundException();
        }

        $category = new $categoryClass;
        $form = $this->createForm($this->getParameter('jt_blog.category.form'), $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $parent->addChild($category);
            $em->flush();

            return $this->redirectToRoute('jt_blog.category_index');
        }

        return $this->render('JTBlogBundle:category:new.html.twig', array(
            'category' => $category,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Category entity.
     *
     * @Route("/{id}/edit", name="jt_blog.category_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, $id)
    {
        $this->denyAccessUnlessGranted($this->getParameter('jt_blog.category.role'));

        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository($this->getCategoryClass())->find($id);
        if ($category === null){
            throw $this->createNotFoundException();
        }

        $deleteForm = $this->createDeleteForm($category);
        $editForm = $this->createForm($this->getParameter('jt_blog.category.form'), $category);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('jt_blog.category_edit', array('id' => $category->getId()));
        }

        return $this->render('JTBlogBundle:category:edit.html.twig', array(
            'category' => $category,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Category entity.
     *
     * @Route("/{id}", name="jt_blog.category_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $this->denyAccessUnlessGranted($this->getParameter('jt_blog.category.role'));

        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository($this->getCategoryClass())->find($id);

        $form = $this->createDeleteForm($category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->remove($category);
            $em->flush();
        }

        return $this->redirectToRoute('jt_blog.category_index');
    }

    private function getCategoryClass()
    {
        return $this->getParameter('jt_blog.category.class');
    }

    /**
     * Creates a form to delete a Category entity.
     *
     * @param Category $category The Category entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Category $category)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('jt_blog.category_delete', array('id' => $category->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
