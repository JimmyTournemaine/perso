<?php
namespace JT\BlogBundle\Censure;

use Doctrine\ORM\EntityManager;
use JT\BlogBundle\Censure\CensorInterface;
use FOS\CommentBundle\Model\CommentInterface;

class Censor implements CensorInterface
{
    private $manager;
    private $comment;
    private $censored;

    public function __construct(EntityManager $em)
    {
        $this->manager = $em;
    }

    public function censor(CommentInterface $comment)
    {
        $bannedWords = $this->manager->getRepository("JTBlogBundle:BannedWord")->findAll();
        $this->comment = $comment->getBody();

        foreach ($bannedWords as $banned){
            $this->censored = str_replace($banned->getWord(), '[censored]', $this->comment);
        }
    }

    public function isValid()
    {
        if ($this->censored === null){
            throw new \LogicException('You have to call ' . self::class . '::censor(CommentInterface $comment) before this test.');
        }

        return $this->censored == $this->comment;
    }
}