<?php
namespace JT\BlogBundle\Censure;

use FOS\CommentBundle\Model\CommentInterface;

interface CensorInterface
{
    public function censor(CommentInterface $comment);
    public function isValid();
}