<?php
namespace JT\BlogBundle\Model;

use FOS\CommentBundle\Model\SignedCommentInterface as BaseSigned;

interface SignedCommentInterface extends BaseSigned {}