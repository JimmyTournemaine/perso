<?php
namespace JT\BlogBundle\Model;

interface CategoryInterface
{
    public function getName();
}