<?php
namespace JT\BlogBundle\Model;

interface AuthorInterface
{
    public function getAuthorName();
}