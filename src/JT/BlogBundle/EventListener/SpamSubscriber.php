<?php
namespace JT\BlogBundle\EventListener;

use FOS\CommentBundle\Event\CommentPersistEvent;
use JT\BlogBundle\SpamDetection\SpamDetectorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use FOS\CommentBundle\Events;
use FOS\CommentBundle\Event\CommentEvent;

class SpamSubscriber implements EventSubscriberInterface
{
    private $detector;

    public function __construct(SpamDetectorInterface $spamDetector)
    {
        $this->detector = $spamDetector;
    }

    public static function getSubscribedEvents()
    {
        return array(
            Events::COMMENT_PRE_PERSIST => array('onFoscommentCommentPrepersist'),
            Events::COMMENT_POST_PERSIST => array('onFoscommentCommentPostpersist')
        );
    }

    public function onFoscommentCommentPrepersist(CommentPersistEvent $event)
    {
        if ($event->isPersistenceAborted())
            return;

        if (true === $this->detector->isSpam($event->getComment())){
            $event->abortPersistence();
        }
    }

    public function onFoscommentCommentPostpersist(CommentEvent $event)
    {
        $this->detector->register($event->getComment());
    }
}