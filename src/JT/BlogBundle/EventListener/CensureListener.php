<?php
namespace JT\BlogBundle\EventListener;

use FOS\CommentBundle\Event\CommentPersistEvent;
use JT\BlogBundle\Censure\CensorInterface;

class CensureListener
{
    private $censor;

    public function __construct(CensorInterface $censor)
    {
        $this->censor = $censor;
    }

    public function onFoscommentCommentPrepersist(CommentPersistEvent $event)
    {
        if ($event->isPersistenceAborted())
            return;

            $this->censor->censor($event->getComment());
        if (false === $this->censor->isValid()){
            $event->abortPersistence();
        }
    }
}