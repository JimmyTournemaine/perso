<?php
namespace BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JT\BlogBundle\Entity\Comment as BaseComment;

/**
 *
 * @author Jimmy Tournemaine <jimmy.tournemaine@yahoo.fr>
 * @ORM\Entity
 * @ORM\Table(name="blog_comment")
 * @ORM\ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 */
class Comment extends BaseComment
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="BlogBundle\Entity\Thread")
     */
    protected $thread;
}