<?php
namespace BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JT\BlogBundle\Entity\Category as BaseCategory;
use JT\BlogBundle\Model\CategoryInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="blog_category")
 */
class Category extends BaseCategory implements CategoryInterface
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
	
	/**
	 * {@inheritDoc}
	 * @see \JT\BlogBundle\Model\CategoryInterface::getName()
	 */
    
	public function getName() {
		parent::getName();
	}

}