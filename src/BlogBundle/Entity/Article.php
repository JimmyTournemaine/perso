<?php
namespace BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JT\BlogBundle\Entity\Article as BaseArticle;

/**
 * @ORM\Entity
 * @ORM\Table(name="blog_article")
 * @author Jimmy Tournemaine <jimmy.tournemaine@yahoo.fr>
 */
class Article extends BaseArticle
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="BlogBundle\Entity\Category")
     * @ORM\JoinColumn(name="category", onDelete="SET NULL")
     */
    protected $category;
}