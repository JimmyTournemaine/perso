<?php

namespace AppBundle\Repository;

use Doctrine\ORM\Query\Expr;
/**
 * ProjectRepository
 */
class ProjectRepository extends \Doctrine\ORM\EntityRepository
{
	public function findPage($page, $nbPerPage, $category = null)
	{
		/*  SELECT * FROM `project` p
			LEFT JOIN category c1 ON p.category_id = c1.id
			LEFT JOIN category c2 ON c1.parent_id = c2.id
			WHERE c1.id = 1 OR c2.id = 1
		 */
		$qb = $this->createQueryBuilder('p');

		if ($category !== null) {
			$qb->leftJoin('AppBundle:Category', 'c1', Expr\Join::WITH, 'c1 = p.category');
			$qb->leftJoin('AppBundle:Category', 'c2', Expr\Join::WITH, 'c2 = c1.parent');
			$qb->where('c1 = :category');
			$qb->orWhere('c2 = :category');
			$qb->setParameter('category', $category);
		}

		return $qb
		    ->andWhere('p.published = true')
			->orderBy('p.publishedAt', 'DESC')
			->setFirstResult(($page-1) * $nbPerPage)
			->setMaxResults($nbPerPage)
			->getQuery()
			->getResult()
		;
	}

	public function countProjects($category = null)
	{
		$qb = $this
			->createQueryBuilder('p')
			->select('count(p)')
			->where('p.published = true')
		;

		if ($category !== null) {
			$qb->andWhere('p.category = :category');
			$qb->setParameter('category', $category);
		}

		return $qb->getQuery()->getSingleScalarResult();
	}
}
