<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EstimateContactType extends AbstractEstimateType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		parent::buildform($builder, $options);

        $builder
            ->add('name', TextType::class, array('label' => 'estimate.form.contact.name'))
        	->add('email', EmailType::class, array('label' => 'estimate.form.contact.email'))
        	->add('company', TextType::class, array(
				'label' => 'estimate.form.contact.company',
				'required' => false
			))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefault('validation_groups', array('contact', 'Default'));
    }
}
