<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Estimate;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EstimateAppType extends AbstractEstimateType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		parent::buildForm($builder, $options);

        $builder
            ->add('quality', ChoiceType::class, array(
					'label' => 'estimate.form.app.quality',
        			'choices' => Estimate::labelQualities(),
        			'expanded' => true,
        	))
        	->add('os', ChoiceType::class, array(
					'label' => 'estimate.form.app.os',
        			'choices' => Estimate::labelOS(),
        			'expanded' => true,
        			'multiple' => true,
        	))
        	->add('design', ChoiceType::class, array(
					'label' => 'estimate.form.app.design',
        			'choices' => Estimate::labelDesign(),
        			'expanded' => true
        	))
        	->add('profits', ChoiceType::class, array(
					'label' => 'estimate.form.app.profits',
        			'choices' => Estimate::labelProfits(),
        			'expanded' => true
        	))
        	->add('profitsOther', TextType::class, array(
					'label' => false,
			))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefault('validation_groups', array('app', 'Default'));
    }
}
