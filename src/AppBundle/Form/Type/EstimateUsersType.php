<?php
namespace AppBundle\Form\Type;

use AppBundle\Entity\Estimate;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EstimateUsersType extends AbstractEstimateType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		parent::buildForm($builder, $options);

        $builder
            ->add('authentication', ChoiceType::class, array(
                    'label' => 'estimate.form.users.authentication',
        			'choices' => Estimate::labelAuthentication(),
        			'expanded' => true
        	))
        	->add('userProfile', ChoiceType::class, array(
        	        'choices' => Estimate::labelProfiles(),
                    'label' => 'estimate.form.users.user_profile',
        			'required' => false
        	))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefault('validation_groups', array('users', 'Default'));
    }
}
