<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use AppBundle\Entity\User;

class ContactType extends AbstractType
{
	private $user;

	public function __construct(TokenStorage $token) {
		$this->user = $token->getToken()->getUser();
	}

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array('label' => 'contact.label.name', 'data' => ($this->user instanceof User) ? $this->user->getName() : null))
            ->add('email', EmailType::class, array('label' => 'contact.label.email', 'data' => ($this->user instanceof User) ? $this->user->getEmail() : null))
            ->add('subject', TextType::class, array('label' => 'contact.label.subject'))
            ->add('content', CKEditorType::class, array('label' => 'contact.label.content'))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Mail\ContactEmail'
        ));
    }
}
