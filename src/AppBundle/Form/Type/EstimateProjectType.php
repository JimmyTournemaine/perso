<?php
namespace AppBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EstimateProjectType extends AbstractEstimateType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		parent::buildForm($builder, $options);

        $builder
            ->add('projectName', TextType::class, array('label' => 'estimate.form.project.name'))
        	->add('projectResume', TextType::class, array(
				'label' => 'estimate.form.project.resume',
        		'required' => false
        	))
        	->add('projectDescription', TextareaType::class, array(
				'label' => 'estimate.form.project.description',
        		'required' => false
        	))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefault('validation_groups', array('project', 'Default'));
    }
}
