<?php
namespace AppBundle\Form\Type;

use AppBundle\Entity\Estimate;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EstimateSystemsType extends AbstractEstimateType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
		parent::buildForm($builder, $options);

        $builder
            ->add('website', ChoiceType::class, array(
                    'label' => 'estimate.form.systems.website',
    			    'choices' => Estimate::labelWebsites(),
                    'placeholder' => null,
        	))
        	->add('websiteUrl', UrlType::class, array(
        	       'label' => 'estimate.form.systems.websiteUrl',
        	))
        	->add('admin', ChoiceType::class, array(
                    'label' => 'estimate.form.systems.admin',
        			'choices' => Estimate::labelAdmins(),
        	))
        	->add('languages', IntegerType::class, array(
        	       'label' => 'estimate.form.systems.languages',
        	))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        parent::configureOptions($resolver);
        $resolver->setDefault('validation_groups', array('systems', 'Default'));
    }
}
