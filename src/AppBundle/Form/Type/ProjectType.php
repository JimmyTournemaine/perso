<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Form\Subscriber\AddPasswordSubscriber;

class ProjectType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        	->add('thumbnail', UrlType::class)
            ->add('name', TextType::class)
            ->add('brief', TextType::class)
            ->add('description', CKEditorType::class)
            ->add('downloadUrl', TextType::class)
            ->add('buyUrl', TextType::class)
            ->add('demoUrl', TextType::class)
            ->add('category', EntityType::class, array(
            		'class' => 'AppBundle\Entity\Category',
            ))
            ->addEventSubscriber(new AddPasswordSubscriber())
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Project'
        ));
    }
}
