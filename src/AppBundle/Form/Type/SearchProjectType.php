<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class SearchProjectType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category', EntityType::class, array(
            		'class' => 'AppBundle\Entity\Category',
            		'query_builder' => function(EntityRepository $er) {
            			return $er->createQueryBuilder('c')->orderBy('c.name');
            		},
            		'label' => 'project.search.categories',
            		'expanded' => true,
            		'multiple' => true,
            ))
            ->add('filters', ChoiceType::class, array(
            		'label' => 'project.search.filters',
            		'attr' => ['placeholder' => 'project.search.placeholder']
            ))
        ;
    }
}
