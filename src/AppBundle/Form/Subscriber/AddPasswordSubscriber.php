<?php
namespace AppBundle\Form\Subscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class AddPasswordSubscriber implements EventSubscriberInterface
{

    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_SET_DATA => 'onPreSet',
            FormEvents::PRE_SUBMIT => 'onPreSubmit'
        );
    }

    public function onPreSet(FormEvent $event)
    {
        $event->getForm()
            ->add('published', CheckboxType::class, array('data' => true))
        ;
    }

    public function onPreSubmit(FormEvent $event)
    {
        $form = $event->getForm();
        $data = $event->getData();
        if (array_key_exists('published', $data)){
            unset($data['password']);
            $form->remove('password');
            $event->setData($data);
        } else {
            $form->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'constraints' => array(new NotBlank(), new Length(array('min' => 6))),
                'first_name' => 'password',
                'second_name' => 'confirm_password'
            ));
        }
    }
}

