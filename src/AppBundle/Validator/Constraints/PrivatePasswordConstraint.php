<?php
namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class PrivatePasswordConstraint extends Constraint
{
    public $message = 'project.private.constraint.password';

    public $password;

    public function __construct($password, $options = null)
    {
        parent::__construct($options);
        $this->password = $password;
    }
}