<?php
namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;

class PrivatePasswordConstraintValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof PrivatePasswordConstraint){
            throw new \LogicException();
        }
        if ($value != $constraint->password){
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}