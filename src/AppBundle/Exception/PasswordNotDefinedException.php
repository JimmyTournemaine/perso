<?php
namespace AppBundle\Exception;

class PasswordNotDefinedException extends \LogicException
{
    public $message = 'A password needs to be defined for private access.';
}