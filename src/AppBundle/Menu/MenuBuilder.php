<?php
namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\Request;
use Knp\Menu\ItemInterface;

class MenuBuilder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    private $checker;
    private $user;

    public function mainMenu(FactoryInterface $factory, array $options)
    {
    	$this->checker = $this->container->get('security.authorization_checker');
    	$this->user = $this->container->get('security.token_storage')->getToken()->getUser();

        $menu = $factory->createItem('root');
		$menu->setChildrenAttribute('class', 'nav navbar-nav pull-right');

		/* Menu for everyone */
        $menu->addChild('home', array('route' => 'homepage', 'label'=>'menu.home'));
        $menu->addChild('projects', array('route' => 'project_index', 'label'=>'menu.projects'));

        $this->translationMenu($menu);
        $this->spentMenu($menu);
        $this->userMenu($menu);

        /* Set languages */
        $request = $this->container->get('request_stack')->getCurrentRequest();
        $params = array_merge($request->get('_route_params'), $request->query->all());

		$labels = $this->container->getParameter('locales_labels');

        $languages = $menu
        	->addChild('language', array('label' => $labels[$request->getLocale()] ))
        	->setAttribute('dropdown', true);

        foreach ($labels as $locale => $label)
        {
        	if ($locale != $request->getLocale()) {
        		$params['_locale'] = $locale;
        		$languages->addChild("locale_$locale", array(
        				'route' => $request->get('_route'),
        				'routeParameters' => $params,
        				'label'=> ucfirst($label)
        		));
        	}
        }

        return $menu;
    }

    private function userMenu(ItemInterface $menu)
    {
    	if(!$this->checker->isGranted('IS_AUTHENTICATED_REMEMBERED'))
    		$menu->addChild('login', array('route' => 'fos_user_security_login', 'label'=>'menu.login'));
    	else {
    		$user = $menu->addChild('user', array('label'=> ucfirst($this->user->getUsername())));
    		$user->setAttribute('dropdown', true);
    		$user->addChild('profile', array('route' => 'fos_user_profile_show', 'label'=> 'menu.profile'))
    			->setAttribute('icon', 'fa fa-user');
    		$user->addChild('logout', array('route' => 'fos_user_security_logout', 'label'=>'menu.logout'))
    			->setAttribute('icon', 'fa fa-power-off');
    	}
    }

    private function translationMenu(ItemInterface $menu)
    {
    	if ($this->checker->isGranted('ROLE_ADMIN'))
    		$menu->addChild('admin', array('route' => 'admin_dashboard', 'label'=>'menu.admin'));
    	if($this->checker->isGranted('ROLE_TRANSLATOR')){
    		$trans = $menu->addChild('trans', array('route' => 'trans_dashboard', 'label'=>'menu.trans'));
    		$trans->setAttribute('dropdown', true);
    		$trans->addChild('trans_dashboard', array('route' => 'trans_dashboard', 'label' => 'Dashboard'))
    			->setAttribute('icon', 'fa fa-dashboard');
    		$trans->addChild('project', array('route' => 'trans_projects', 'label' => 'menu.projects'))
    			->setAttribute('icon', 'fa fa-file-text');
    	}
    }

    private function spentMenu(ItemInterface $menu)
    {
        if(!$this->checker->isGranted('ROLE_SPENT')){
            return;
        }

        $spentMenu = $menu->addChild('spent', array('label' => 'Dépenses'));
        $spentMenu->setAttribute('dropdown', true);

        $spentMenu->addChild('spent_index', array('route' => 'spent_index', 'label'=>'Dépenses'))->setAttribute('icon', 'fa fa-credit-card');
        $spentMenu->addChild('spent_subscription_index', array('route' => 'spent_subscription_index', 'label'=>'Abonnements'))->setAttribute('icon', 'fa fa-calendar');
        $spentMenu->addChild('spent_settlement_archives', array('route' => 'spent_settlement_archives', 'label'=>'Anciens Réglements'))->setAttribute('icon', 'fa fa-money');
    }
}
