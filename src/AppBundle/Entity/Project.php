<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Translatable\Translatable;
use AppBundle\Entity\Category;
use AppBundle\Model\Hidden;

/**
 * Project
 *
 * @ORM\Table(name="project")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProjectRepository")
 */
class Project extends Hidden implements Translatable
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=63, unique=true)
     * @Assert\NotBlank()
     * @Assert\Length(min=3, max=63)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     * @Gedmo\Slug(fields={"name"})
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="brief", type="string", length=255)
     * @Gedmo\Translatable()
     * @Assert\NotBlank()
     */
    private $brief;

    /**
     * @var string
     *
     * @ORM\Column(name="thumbnail", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Url()
     */
    private $thumbnail;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     * @Gedmo\Translatable
     * @Assert\NotBlank()
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="Category")
     */
    private $category;

    /**
     * @Gedmo\Locale
     */
    private $locale;

    /**
     * @ORM\Column(name="download_url", type="string", length=255, nullable=true)
     * @Assert\Url()
     */
    private $downloadUrl;

    /**
     * @ORM\Column(name="buy_url", type="string", length=255, nullable=true)
     * @Assert\Url()
     */
    private $buyUrl;

    /**
     * @ORM\Column(name="demo_url", type="string", length=255, nullable=true)
     * @Assert\Url()
     */
    private $demoUrl;

    /**
     * @ORM\Column(name="updatedAt", type="datetime")
     * @Gedmo\timestampable(on="update")
     */
    private $updatedAt;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Project
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Project
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Project
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    public function setTranslatableLocale($locale)
    {
    	$this->locale = $locale;
    }

    /**
     * Set brief
     *
     * @param string $brief
     *
     * @return Project
     */
    public function setBrief($brief)
    {
        $this->brief = $brief;

        return $this;
    }

    /**
     * Get brief
     *
     * @return string
     */
    public function getBrief()
    {
        return $this->brief;
    }

    /**
     * Set thumbnail
     *
     * @param string $thumbnail
     *
     * @return Project
     */
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     * Get thumbnail
     *
     * @return string
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * Set downloadUrl
     *
     * @param string $downloadUrl
     *
     * @return Project
     */
    public function setDownloadUrl($downloadUrl)
    {
        $this->downloadUrl = $downloadUrl;

        return $this;
    }

    /**
     * Get downloadUrl
     *
     * @return string
     */
    public function getDownloadUrl()
    {
        return $this->downloadUrl;
    }

    /**
     * Set buyUrl
     *
     * @param string $buyUrl
     *
     * @return Project
     */
    public function setBuyUrl($buyUrl)
    {
        $this->buyUrl = $buyUrl;

        return $this;
    }

    /**
     * Get buyUrl
     *
     * @return string
     */
    public function getBuyUrl()
    {
        return $this->buyUrl;
    }

    /**
     * Set demoUrl
     *
     * @param string $demoUrl
     *
     * @return Project
     */
    public function setDemoUrl($demoUrl)
    {
        $this->demoUrl = $demoUrl;

        return $this;
    }

    /**
     * Get demoUrl
     *
     * @return string
     */
    public function getDemoUrl()
    {
        return $this->demoUrl;
    }

    public function setLocale($locale)
    {
    	$this->locale = $locale;

    	return $this;
    }

    public function getLocale()
    {
    	return $this->locale;
    }

    /**
     * Set category
     *
     * @param \AppBundle\Entity\Category $category
     *
     * @return Project
     */
    public function setCategory(Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \AppBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Project
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
