<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Gedmo\Mapping\Annotation as Gedmo;
use JT\BlogBundle\Model\AuthorInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 * @author Jimmy Tournemaine <jimmy.tournemaine@yahoo.fr>
 */
class User extends BaseUser implements AuthorInterface
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @ORM\Column(type="boolean")
	 */
	private $quotable;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $name;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $url;

	/**
	 * @ORM\Column(name="signed_out_at", type="datetime")
	 * @Gedmo\Timestampable(on="create")
	 */
	private $signedOutAt;

	public function __construct()
	{
		parent::__construct();
		$this->quotable = false;
	}

	public function getAuthorName()
	{
	    return ($this->name === null) ? $this->username : $this->name;
	}

    /**
     * Set isQuotable
     *
     * @param boolean $isQuotable
     *
     * @return User
     */
    public function setQuotable($isQuotable)
    {
        $this->quotable = $isQuotable;

        return $this;
    }

    /**
     * Get isQuotable
     *
     * @return boolean
     */
    public function isQuotable()
    {
        return $this->quotable;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return User
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set signedOutAt
     *
     * @param \DateTime $signedOutAt
     *
     * @return User
     */
    public function setSignedOutAt($signedOutAt)
    {
        $this->signedOutAt = $signedOutAt;

        return $this;
    }

    /**
     * Get signedOutAt
     *
     * @return \DateTime
     */
    public function getSignedOutAt()
    {
        return $this->signedOutAt;
    }
}
