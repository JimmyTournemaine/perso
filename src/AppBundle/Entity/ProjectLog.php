<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectLog
 *
 * @ORM\Table(name="project_logs")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProjectLogRepository")
 */
class ProjectLog
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;
    
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Project")
     * @ORM\JoinColumn(nullable=false)
     */
    private $project;
    
    /**
     * @ORM\Column(name="old_brief", type="string", length=255, nullable=true)
     */
    private $oldBrief;
    
    /**
     * @ORM\Column(name="old_description", type="text", nullable=true)
     */
    private $oldDescription;
    
    /**
     * @ORM\Column(name="locale", type="string", length=5)
     */
    private $locale;

    /**
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    public function __construct() {
    	$this->updatedAt = new \DateTime();
    }
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return ProjectLog
     */
    public function setUser(\AppBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set project
     *
     * @param \AppBundle\Entity\User $project
     *
     * @return ProjectLog
     */
    public function setProject(\AppBundle\Entity\Project $project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \AppBundle\Entity\User
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set oldBrief
     *
     * @param string $oldBrief
     *
     * @return ProjectLog
     */
    public function setOldBrief($oldBrief)
    {
        $this->oldBrief = $oldBrief;

        return $this;
    }

    /**
     * Get oldBrief
     *
     * @return string
     */
    public function getOldBrief()
    {
        return $this->oldBrief;
    }

    /**
     * Set oldDescription
     *
     * @param string $oldDescription
     *
     * @return ProjectLog
     */
    public function setOldDescription($oldDescription)
    {
        $this->oldDescription = $oldDescription;

        return $this;
    }

    /**
     * Get oldDescription
     *
     * @return string
     */
    public function getOldDescription()
    {
        return $this->oldDescription;
    }

    /**
     * Set locale
     *
     * @param string $locale
     *
     * @return ProjectLog
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get locale
     *
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return ProjectLog
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
