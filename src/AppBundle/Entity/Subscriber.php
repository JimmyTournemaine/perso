<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Subscriber
 *
 * @ORM\Table(name="subscriber")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SubscriberRepository")
 * @UniqueEntity("email")
 */
class Subscriber
{
    /**
     * @var string
     * @ORM\Id
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     * @Assert\Email(strict=true, checkMX=true)
     * @Assert\NotBlank()
     * @Assert\NotNull()
     */
    private $email;

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Subscriber
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
}
