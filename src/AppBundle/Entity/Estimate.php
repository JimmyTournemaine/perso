<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="estimate")
 * @author Jimmy Tournemaine <jimmy.tournemaine@yahoo.fr>
 */
class Estimate
{
	const QUALITY_ANY = 0;
	const QUALITY_RATIO = 1;
	const QUALITY_OPTI = 2;
	const OS_ANDROID = 0;
	const OS_IOS = 1;
	const OS_WINDOWS = 2;
	const DESIGN_SIMPLE = 0;
	const DESIGN_PERSO = 1;
	const DESIGN_WEB = 2;
	const DESIGN_NONE = 3;
	const PROFITS_AD = 0;
	const PROFITS_PAID = 1;
	const PROFITS_INAPP = 2;
	const PROFITS_OTHER = 3;
	const WEBSITE_YES = 1;
	const WEBSITE_NO = 2;
	const WEBSITE_UNKNOWN = 3;
	const ADMIN_YES = 1;
	const ADMIN_NO = 2;
	const ADMIN_UNKNOWN = 3;
	const AUTHENTICATION_NO = 0;
	const AUTHENTICATION_EMAIL = 1;
	const AUTHENTICATION_SOCIAL = 2;
	const AUTHENTICATION_UNKNOWN = 3;
	const PROFILE_YES = 1;
	const PROFILE_NO = 2;
	const PROFILE_UNKNOWN = 3;

	/**
	 * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

	/**
	 * @ORM\Column(name="username", type="string", length=255)
	 * @Assert\NotBlank(groups={"contact"})
	 * @Assert\Length(max=255)
	 */
	private $name;

	/**
	 * @ORM\Column(name="email", type="string", length=180)
	 * @Assert\NotBlank(groups={"contact"})
	 * @Assert\Email()
	 * @Assert\Length(max=180)
	 */
	private $email;

	/**
	 * @ORM\Column(name="company", type="string", length=255, nullable=true)
	 * @Assert\Length(max=255)
	 */
	private $company;

	/**
	 * @ORM\Column(name="project_name", type="string", length=64)
	 * @Assert\NotBlank(groups={"project"})
	 * @Assert\Length(max=64)
	 */
	private $projectName;

	/**
	 * @ORM\Column(name="project_resume", type="string", length=255)
	 * @Assert\NotBlank(groups={"project"})
	 * @Assert\Length(max=255)
	 */
	private $projectResume;

	/**
	 * @ORM\Column(name="project_description", type="text", nullable=true)
	 * @Assert\Length(min=64)
	 */
	private $projectDescription;

	/**
	 * @ORM\Column(name="quality", type="smallint")
	 * @Assert\NotBlank(groups={"app"})
     * @Assert\Choice(callback = "getQualities")
	 */
	private $quality;

	/**
	 * @ORM\Column(name="os", type="array")
	 * @Assert\NotBlank(groups={"app"})
     * @Assert\Choice(callback = "getOss", multiple=true)
	 */
	private $os;

	/**
	 * @ORM\Column(name="design", type="smallint")
	 * @Assert\NotBlank(groups={"app"})
     * @Assert\Choice(callback = "getDesigns")
	 */
	private $design;

	/**
	 * @ORM\Column(name="profits", type="smallint")
	 * @Assert\NotBlank(groups={"app"})
     * @Assert\Choice(callback = "getProfitsChoices")
	 */
	private $profits;

	/**
	 * @ORM\Column(name="profits_other", type="string", length=255, nullable=true)
	 */
	private $profitsOther;

	/**
	 * @ORM\Column(name="website", type="smallint")
     * @Assert\Choice(callback = "getWebsites")
	 */
	private $website;

	/**
	 * @ORM\Column(name="website_url", type="string", length=255, nullable=true)
	 * @Assert\Url(protocols = {"http", "https"}, checkDNS = true, groups={"systems"})
	 */
	private $websiteUrl;

	/**
	 * @ORM\Column(name="admin", type="smallint")
     * @Assert\Choice(callback = "getAdmins")
	 */
	private $admin;


	/**
	 * @ORM\Column(name="languages", type="smallint")
	 * @Assert\NotBlank(groups={"systems"})
	 * @Assert\GreaterThanOrEqual(1)
	 */
	private $languages = 1;


	/**
	 * @ORM\Column(name="authentication", type="smallint")
	 * @Assert\NotBlank(groups={"users"})
     * @Assert\Choice(callback = "getAuthentications")
	 */
	private $authentication;


	/**
	 * @ORM\Column(name="profile", type="smallint")
     * @Assert\Choice(callback = "getProfiles")
	 */
	private $userProfile;

	/**
	 * @Assert\IsTrue(groups={"app"}, message="The economic model cannot be blank.")
	 */
	public function isProfitsOtherValid()
	{
	    return ($this->profits !== self::PROFITS_OTHER) || !empty($this->profitsOther);
	}

	/**
	 * @Assert\IsTrue(groups={"systems"}, message="The website URL cannot be blank.")
	 */
	public function isWebsiteUrlValid()
	{
	    return $this->website !== self::WEBSITE_YES || !empty($this->websiteUrl);
	}

	/**
	 * @Assert\IsTrue(groups={"users"}, message="Please select an option for the user's profile.")
	 */
	public function isUserProfileValid()
	{
	    return $this->authentication === self::AUTHENTICATION_NO || $this->userProfile !== null;
	}

	static public function getQualities()
	{
	    return array(self::QUALITY_ANY, self::QUALITY_RATIO, self::QUALITY_OPTI);
	}
	static public function labelQualities()
	{
	    return array(
	        'estimate.label.qualities.any' => self::QUALITY_ANY,
	        'estimate.label.qualities.ratio' => self::QUALITY_RATIO,
	        'estimate.label.qualities.opti' => self::QUALITY_OPTI,
	    );
	}
	public function printQuality()
	{
	    return array_search($this->quality, self::labelQualities());
	}
	static public function getOss()
	{
	    return array(self::OS_ANDROID, self::OS_IOS, self::OS_WINDOWS);
	}
	static public function labelOS()
	{
	    return array(
	        'estimate.label.os.android' => self::OS_ANDROID,
	        'estimate.label.os.ios' => self::OS_IOS,
	        'estimate.label.os.windows' => self::OS_WINDOWS,
	    );
	}
	public function printOS()
	{
	    $os = array();
	    $list = self::labelOS();
	    foreach ($this->os as $sys){
	        array_push($os, array_search($sys, $list));
	    }
	    return $os;
	}
	static public function getDesigns()
	{
	    return array(self::DESIGN_SIMPLE, self::DESIGN_PERSO, self::DESIGN_WEB, self::DESIGN_NONE);
	}
	static public function labelDesign()
	{
	    return array(
	        'estimate.label.design.simple' => self::DESIGN_SIMPLE,
	        'estimate.label.design.perso' => self::DESIGN_PERSO,
	        'estimate.label.design.web' => self::DESIGN_WEB,
	        'estimate.label.design.none' => self::DESIGN_NONE,
	    );
	}
	public function printDesign()
	{
	    return array_search($this->design, self::labelDesign());
	}
    static public function getProfitsChoices()
	{
	    return array(self::PROFITS_AD, self::PROFITS_PAID, self::PROFITS_INAPP, self::PROFITS_OTHER);
	}
	static public function labelProfits()
	{
	    return array(
	        'estimate.label.profits.ad' => self::PROFITS_AD,
	        'estimate.label.profits.paid' => self::PROFITS_PAID,
	        'estimate.label.profits.inapp' => self::PROFITS_INAPP,
	        'estimate.label.profits.other' => self::PROFITS_OTHER,
	    );
	}
	public function printProfits()
	{
	    return array_search($this->profits, self::labelQualities());
	}
    static public function getWebsites()
	{
	    return array(self::WEBSITE_NO, self::WEBSITE_YES, self::WEBSITE_UNKNOWN);
	}
	static public function labelWebsites()
	{
	    return array(
	        'estimate.label.yes' => self::WEBSITE_YES,
	        'estimate.label.no' => self::WEBSITE_NO,
	        'estimate.label.unknown' => self::WEBSITE_UNKNOWN,
	    );
	}
	public function printWebsite()
	{
	    return array_search($this->website, self::labelWebsites());
	}
	static public function getAdmins()
	{
	    return array(self::ADMIN_NO, self::ADMIN_YES, self::ADMIN_UNKNOWN);
	}
	static public function labelAdmins()
	{
	    return array(
	        'estimate.label.yes' => self::ADMIN_YES,
	        'estimate.label.no' => self::ADMIN_NO,
	        'estimate.label.unknown' => self::ADMIN_UNKNOWN,
	    );
	}
	public function printAdmin()
	{
	    return array_search($this->admin, self::labelAdmins());
	}
	static public function getAuthentications()
	{
	    return array(self::AUTHENTICATION_NO, self::AUTHENTICATION_EMAIL, self::AUTHENTICATION_SOCIAL, self::AUTHENTICATION_UNKNOWN);
	}
	static public function labelAuthentication()
	{
	    return array(
	        'estimate.label.authentication.no' => self::AUTHENTICATION_NO,
	        'estimate.label.authentication.email' => self::AUTHENTICATION_EMAIL,
	        'estimate.label.authentication.social' => self::AUTHENTICATION_SOCIAL,
	        'estimate.label.authentication.unknown' => self::AUTHENTICATION_UNKNOWN,
	    );
	}
	public function printAuthentication()
	{
	    return array_search($this->authentication, self::labelAuthentication());
	}
	static public function getProfiles()
	{
	    return array(self::PROFILE_NO, self::PROFILE_YES, self::PROFILE_UNKNOWN);
	}
	static public function labelProfiles()
	{
	    return array(
	        'estimate.label.yes' => self::PROFILE_YES,
	        'estimate.label.no' => self::PROFILE_NO,
	        'estimate.label.unknown' => self::PROFILE_UNKNOWN,
	    );
	}
	public function printProfiles()
	{
	    return array_search($this->userProfile, self::labelProfiles());
	}

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Estimate
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Estimate
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set company
     *
     * @param string $company
     *
     * @return Estimate
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set projectName
     *
     * @param string $projectName
     *
     * @return Estimate
     */
    public function setProjectName($projectName)
    {
        $this->projectName = $projectName;

        return $this;
    }

    /**
     * Get projectName
     *
     * @return string
     */
    public function getProjectName()
    {
        return $this->projectName;
    }

    /**
     * Set projectResume
     *
     * @param string $projectResume
     *
     * @return Estimate
     */
    public function setProjectResume($projectResume)
    {
        $this->projectResume = $projectResume;

        return $this;
    }

    /**
     * Get projectResume
     *
     * @return string
     */
    public function getProjectResume()
    {
        return $this->projectResume;
    }

    /**
     * Set projectDescription
     *
     * @param string $projectDescription
     *
     * @return Estimate
     */
    public function setProjectDescription($projectDescription)
    {
        $this->projectDescription = $projectDescription;

        return $this;
    }

    /**
     * Get projectDescription
     *
     * @return string
     */
    public function getProjectDescription()
    {
        return $this->projectDescription;
    }

    /**
     * Set quality
     *
     * @param integer $quality
     *
     * @return Estimate
     */
    public function setQuality($quality)
    {
        $this->quality = (int) $quality;

        return $this;
    }

    /**
     * Get quality
     *
     * @return integer
     */
    public function getQuality()
    {
        return $this->quality;
    }

    /**
     * Set os
     *
     * @param integer $os
     *
     * @return Estimate
     */
    public function setOs($os)
    {
        $this->os = $os;

        return $this;
    }

    /**
     * Get os
     *
     * @return integer
     */
    public function getOs()
    {
        return $this->os;
    }

    /**
     * Set design
     *
     * @param integer $design
     *
     * @return Estimate
     */
    public function setDesign($design)
    {
        $this->design = (int) $design;

        return $this;
    }

    /**
     * Get design
     *
     * @return integer
     */
    public function getDesign()
    {
        return $this->design;
    }

    /**
     * Set profits
     *
     * @param integer $profits
     *
     * @return Estimate
     */
    public function setProfits($profits)
    {
        $this->profits = (int) $profits;

        return $this;
    }

    /**
     * Get profits
     *
     * @return integer
     */
    public function getProfits()
    {
        return $this->profits;
    }

    /**
     * Set profitsOther
     *
     * @param string $profitsOther
     *
     * @return Estimate
     */
    public function setProfitsOther($profitsOther)
    {
        $this->profitsOther = $profitsOther;
        return $this;
    }

    /**
     * Get profitsOther
     *
     * @return string
     */
    public function getProfitsOther()
    {
        return $this->profitsOther;
    }

    /**
     * Set website
     *
     * @param integer $website
     *
     * @return Estimate
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return integer
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set websiteUrl
     *
     * @param string $websiteUrl
     *
     * @return Estimate
     */
    public function setWebsiteUrl($websiteUrl)
    {
        $this->websiteUrl = $websiteUrl;

        return $this;
    }

    /**
     * Get websiteUrl
     *
     * @return string
     */
    public function getWebsiteUrl()
    {
        return $this->websiteUrl;
    }

    /**
     * Set admin
     *
     * @param integer $admin
     *
     * @return Estimate
     */
    public function setAdmin($admin)
    {
        $this->admin = $admin;

        return $this;
    }

    /**
     * Get admin
     *
     * @return integer
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * Set languages
     *
     * @param integer $languages
     *
     * @return Estimate
     */
    public function setLanguages($languages)
    {
        $this->languages = $languages;

        return $this;
    }

    /**
     * Get languages
     *
     * @return integer
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * Set authentication
     *
     * @param integer $authentication
     *
     * @return Estimate
     */
    public function setAuthentication($authentication)
    {
        $this->authentication = $authentication;

        return $this;
    }

    /**
     * Get authentication
     *
     * @return integer
     */
    public function getAuthentication()
    {
        return $this->authentication;
    }

    /**
     * Set userProfile
     *
     * @param integer $userProfile
     *
     * @return Estimate
     */
    public function setUserProfile($userProfile)
    {
        $this->userProfile = $userProfile;

        return $this;
    }

    /**
     * Get userProfile
     *
     * @return integer
     */
    public function getUserProfile()
    {
        return $this->userProfile;
    }
}
