<?php
namespace AppBundle\Model;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Exception\PasswordNotDefinedException;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 *
 * @author Jimmy Tournemaine <jimmy.tournemaine@yahoo.fr>
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks()
 */
class Hidden implements Hidable
{

    /**
     * @var boolean
     * @ORM\Column(name="published", type="boolean", nullable=false)
     */
    protected $published;

    /**
     * @var string
     * @ORM\Column(name="password", type="string", length=255, nullable=true)
     */
    protected $password;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="publishedAt", type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    protected $publishedAt;

    /**
     * @ORM\PrePersist()
     * @throws PasswordNotDefinedException
     */
    public function check()
    {
        if ($this->published === false && $this->password === null)
            throw new PasswordNotDefinedException();
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \AppBundle\Model\Hidable::isPublic()
     */
    public function isPublished()
    {
        return $this->published;
    }

    /**
     *
     * {@inheritDoc}
     *
     * @see \AppBundle\Model\Hidable::setPublic()
     */
    public function setPublished($boo)
    {
        $this->published = $boo;

        return $this;
    }

    public function getPassword(){
        return $this->password;
    }

    public function setPassword($pass)
    {
        $this->password = $pass;
        return $this;
    }

    /**
     * Set publishedAt
     *
     * @param \DateTime $publishedAt
     *
     * @return Project
     */
    public function setPublishedAt($publishedAt)
    {
        $this->publishedAt = $publishedAt;

        return $this;
    }

    /**
     * Get publishedAt
     *
     * @return \DateTime
     */
    public function getPublishedAt()
    {
        return $this->publishedAt;
    }
}
