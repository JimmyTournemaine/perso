<?php
namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\Session\Session;
use AppBundle\Entity\Estimate;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class EstimateStepSaver
{
    const NAME = 'estimate_saver';

    private $session;

    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    public function handleForm(FormInterface $form, Request $request)
    {
        $this->add($request->get($form->getName()));
    }

    public function getEstimate()
    {
        $estimate = new Estimate();
        foreach ($this->get() as $property => $value)
        {
            $setter = 'set'.ucfirst($property);
            if(method_exists($estimate, $setter)){
                $estimate->$setter($value);
            }
        }
        return $estimate;
    }

    private function add(array $values)
    {
        $this->set(array_merge($this->get(), $values));
    }

    private function get()
    {
        return $this->session->get(self::NAME, array());
    }

    private function set(array $values)
    {
        $this->session->set(self::NAME, $values);
    }
}