<?php
namespace AppBundle\Service;

class Flashify
{
    const TYPE_ERROR = 'danger';
    const TYPE_WARNING = 'warning';
    const TYPE_NOTICE = 'info';
    const TYPE_SUCCESS = 'success';
    const TYPE_INFO = 'info';

    static private $ICONS = array(
        'success' => 'ok-sign',
        'warning' => 'warning-sign',
        'danger' => 'remove-sign',
        'info' => 'info-sign',
    );

    public function toFlash($type, $message, $dismissible = true)
    {
        $flashType = $type;
        if (!in_array($type, array(self::TYPE_ERROR, self::TYPE_NOTICE, self::TYPE_INFO, self::TYPE_SUCCESS, self::TYPE_WARNING))) {
            $flashType = self::TYPE_INFO;
        }

        $flash = '<div class="alert alert-' . $flashType;
        if($dismissible === true){
            $flash .= ' alert-dismissible';
        }
        $flash .= '" role="alert"><i class="glyphicon glyphicon-' . self::$ICONS[$flashType] . '" aria-hidden="true"></i> ' . $message;
        if($dismissible === true){
            $flash .= '<button type="button" class="close fade in" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        }
        $flash .= '</div>';

        return $flash;
    }
}