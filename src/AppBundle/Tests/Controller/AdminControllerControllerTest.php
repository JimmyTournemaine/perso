<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AdminControllerControllerTest extends WebTestCase
{
    public function testDashboard()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/dashboard');
    }

    public function testProjects()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/projects');
    }

    public function testArticles()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/articles');
    }

}
