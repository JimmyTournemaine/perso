<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class EstimateControllerTest extends WebTestCase
{
    public function testContact()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/contact');
    }

    public function testProject()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/project');
    }

    public function testApp()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/app');
    }

    public function testSystems()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/systems');
    }

    public function testUsers()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/users');
    }

    public function testConfirm()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/confirm');
    }

}
