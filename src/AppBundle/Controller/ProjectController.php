<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Project;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use AppBundle\Validator\Constraints\PrivatePasswordConstraint;

/**
 * Project controller.
 *
 * @Route("/project")
 */
class ProjectController extends Controller
{
    /**
     * Lists all Project entities.
     *
     * @Route("/{page}", requirements={"page" = "\d+"}, defaults={"page"=1}, name="project_index")
     * @Method("GET")
     */
    public function indexAction(Request $request, $page)
    {
    	if($page < 1) {
    		throw $this->createNotFoundException();
    	}

    	$nbPerPage = 6;
    	$em = $this->getDoctrine()->getManager();

    	$category = $request->get('category');
    	$categories = $em->getRepository("AppBundle:Category")->findRoots();

    	$repo = $em->getRepository('AppBundle:Project');
    	$nbProjects = $repo->countProjects($category);
    	$nbPages = ceil($nbProjects/$nbPerPage);
    	$projects = $repo->findPage($page, $nbPerPage, $category);


        if($page > $nbPages && $category === null && $nbProjects > 0) {
        	throw $this->createNotFoundException();
        }

        return $this->render('project/index.html.twig', array(
            'projects' => $projects,
        	'categories' => $categories,
        	'category' => $category,
        	'nbPages'  => $nbPages,
        	'page'	   => $page
        ));
    }


    /**
     * Finds and displays a Project entity.
     *
     * @Route("/{slug}", name="project_show")
     * @Method({"GET","POST"})
     */
    public function showAction(Request $request, Project $project)
    {
        $form = $this->createFormBuilder()
            ->add('password', PasswordType::class, array(
                'constraints' => array(new PrivatePasswordConstraint($project->getPassword()))
            ))
            ->getForm()
            ->handleRequest($request);

        if ($project->isPublished() || $form->isValid()){
            return $this->render('project/show.html.twig', array(
                'project' => $project,
            ));
        }

        return $this->render('project/show_private.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
