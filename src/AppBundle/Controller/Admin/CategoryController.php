<?php

namespace AppBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Category;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Category controller.
 *
 * @Route("/category")
 */
class CategoryController extends Controller
{
    /**
     * Lists all Category entities.
     *
     * @Route("/", name="admin_category_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository('AppBundle:Category')->findBy(['parent' => null]);

        return $this->render('category/index.html.twig', array(
            'categories' => $categories,
        ));
    }

    /**
     * Creates a new Category entity.
     *
     * @Route("/new", name="admin_category_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $form = $this->createMultilanguageForm()->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
        	$em = $this->getDoctrine()->getManager();
        	$repository = $em->getRepository('Gedmo\\Translatable\\Entity\\Translation');

        	/* Get Locales */
        	$default_locale = $this->container->getParameter('default_locale');
        	$locales = $this->container->getParameter('locales');
        	$data = $form->getData();

        	/* Translate names */
        	$category = new Category();
        	$category->setName($data[$default_locale]);
        	foreach ($locales as $locale) {
        		if ($locale != $default_locale) {
        			$repository->translate($category, 'name', $locale, $data[$locale]);
        		}
        	}

        	/* Store them */
            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('admin_category_index');
        }

        return $this->render('category/new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new Category entity.
     *
     * @Route("/{id}/add-subcategory", name="admin_category_sub")
     * @Method({"GET", "POST"})
     */
    public function subAction(Request $request, Category $parent)
    {
    	$form = $this->createMultilanguageForm()->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
        	$em = $this->getDoctrine()->getManager();
        	$repository = $em->getRepository('Gedmo\\Translatable\\Entity\\Translation');

        	/* Get Locales */
        	$default_locale = $this->container->getParameter('default_locale');
        	$locales = $this->container->getParameter('locales');
        	$data = $form->getData();

        	/* Translate names */
        	$category = new Category();
        	$category->setParent($parent);
        	$category->setName($data[$default_locale]);
        	foreach ($locales as $locale) {
        		if ($locale != $default_locale) {
        			$repository->translate($category, 'name', $locale, $data[$locale]);
        		}
        	}

        	/* Store them */
            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('admin_category_index');
        }

        return $this->render('category/new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Category entity.
     *
     * @Route("/{id}/edit/{_locale}", name="admin_category_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Category $category)
    {
    	$locale = $request->request->get('_locale');

    	$category->setLocale($locale);
    	$em = $this->getDoctrine()->getManager();
    	$em->refresh($category);

        $deleteForm = $this->createDeleteForm($category);
        $editForm = $this->createForm('AppBundle\Form\CategoryType', $category);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('admin_category_edit', array('id' => $category->getId()));
        }

        return $this->render('category/edit.html.twig', array(
            'category' => $category,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Category entity.
     *
     * @Route("/{id}", name="admin_category_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Category $category)
    {
        $form = $this->createDeleteForm($category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($category);
            $em->flush();
        }

        return $this->redirectToRoute('admin_category_index');
    }

    /**
     * Creates a form to delete a Category entity.
     *
     * @param Category $category The Category entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createMultilanguageForm()
    {
    	$default_locale = $this->container->getParameter('default_locale');
    	$locales_labels = $this->container->getParameter('locales_labels');

    	$fb = $this->createFormBuilder();
    	$fb->add($default_locale, TextType::class, array('label' => ucfirst($locales_labels[$default_locale])));
    	foreach ($locales_labels as $locale => $label) {
    		if ($locale != $default_locale) {
    			$fb->add($locale, TextType::class, array('label' => ucfirst($label)));
    		}
    	}

    	return $fb->getForm();
    }

    /**
     * Creates a form to delete a Category entity.
     *
     * @param Category $category The Category entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Category $category)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_category_delete', array('id' => $category->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
