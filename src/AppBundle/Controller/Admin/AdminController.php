<?php

namespace AppBundle\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AdminController extends Controller
{
    /**
     * @Route("/", name="admin_dashboard")
     */
    public function dashboardAction()
    {
        return $this->render('admin/dashboard.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/projects", name="admin_projects")
     */
    public function projectsAction()
    {
    	$em = $this->getDoctrine()->getManager();
    	$projects = $em->getRepository("AppBundle:Project")->findAll();
    	
        return $this->render('admin/projects.html.twig', array(
            'projects' => $projects,
        ));
    }
}
