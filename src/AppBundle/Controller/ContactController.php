<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Form\Type\ContactType;
use AppBundle\Mail\ContactEmail;
use Symfony\Component\HttpFoundation\Request;

class ContactController extends Controller
{
    /**
     * @Route("/contact", name="contact")
     * @Method({"GET", "POST"})
     */
    public function contactAction(Request $request)
    {
    	$email = new ContactEmail();
    	$form = $this
    		->createForm(ContactType::class, $email)
    		->handleRequest($request);

    	if ($form->isValid()) {
    		/* Check Captcha */
    		$captcha = $request->get('g-recaptcha-response');
    		if(!$captcha){
				$this->addRecaptchaError($form);
    		} else {
				$secret = "6LfpPRsTAAAAALumUgyuQ_z_THYxeUg56TBF7dMg";
				$ip = $request->getClientIp();
				$response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$captcha."&remoteip=".$ip);
				$responseKeys = json_decode($response,true);

				if(intval($responseKeys["success"]) !== 1) {
					$this->addRecaptchaError($form);
				}
			}

    		/* Send mail */
    		if($form->getErrors()->count() == 0)
			{
				if ($this->sendEmail($email)) {
					$this->addFlash('success', $this->get('translator')->trans('flash.contact.mail_send'));
					return $this->redirectToRoute('homepage');
				}
				else {
					$this->addFlash('danger', $this->get('translator')->trans('flash.contact.mail_notsend'));
				}
			}
    	}

        return $this->render('contact/contact.html.twig', array(
            'form' => $form->createView(),
        ));
    }

	private function addRecaptchaError($form)
	{
		$error = $this->get('translator')->trans('contact.error.recaptcha').'.';
		$form->addError(new FormError($error));
	}

    protected function sendEmail($email)
    {
    	$message = \Swift_Message::newInstance($email->getSubject(), $email->getContent(), 'text/html')
    		->setFrom($email->getEmail(), $email->getName())
    		->setTo($this->getParameter('contact_address'));

    	return $this->get('mailer')->send($message);
    }

}
