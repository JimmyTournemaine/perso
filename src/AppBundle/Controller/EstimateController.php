<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Form\Type\EstimateContactType;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\Type\EstimateProjectType;
use AppBundle\Form\Type\EstimateAppType;
use AppBundle\Form\Type\EstimateSystemsType;
use AppBundle\Form\Type\EstimateUsersType;

/**
 * @Route("/estimate")
 * @author Jimmy Tournemaine <jimmy.tournemaine@yahoo.fr>
 */
class EstimateController extends Controller
{
    /**
     * @Route("/{step}", name="estimate_form", requirements={"step": "\d"}, defaults={"step": 1})
     */
    public function contactAction(Request $request, $step)
    {
        switch($step) {
            case 1:
                $formType = EstimateContactType::class;
                $template = 'estimate/contact.html.twig';
                break;
            case 2:
                $formType = EstimateProjectType::class;
                $template = 'estimate/project.html.twig';
                break;
            case 3:
                $formType = EstimateAppType::class;
                $template = 'estimate/app.html.twig';
                break;
            case 4:
                $formType = EstimateSystemsType::class;
                $template = 'estimate/systems.html.twig';
                break;
            case 5:
                $formType = EstimateUsersType::class;
                $template = 'estimate/users.html.twig';
                break;
            default:
                throw new \InvalidArgumentException("There is only steps 1-5.");
        }


        $saver = $this->get('app.estimate_step_saver');
        $estimate = $saver->getEstimate();

        $form = $this->createForm($formType, $estimate)->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $saver->handleForm($form, $request);
            if($step == 5) {
                return $this->redirectToRoute('estimate_confirm');
            }
            return $this->redirectToRoute('estimate_form', array('step' => $step+1));
        }

        return $this->render($template, array(
            'form' => $form->createView(),
            'step' => $step,
        ));
    }

    /**
     * @Route("/confirm", name="estimate_confirm")
     */
    public function confirmAction(Request $request)
    {
        $estimate = $this->get('app.estimate_step_saver')->getEstimate();
		$form = $this->createFormBuilder()->getForm()->handleRequest($request);

		if($form->isSubmitted() && $form->isValid())
		{
			$em = $this->getDoctrine()->getManager();
			$em->persist($estimate);
			$em->flush();
			$this->addFlash('success', $this->get('translator')->trans('estimate.flash.success'));
			return $this->redirectToRoute('homepage');
		}

        return $this->render('estimate/confirm.html.twig', array(
            'estimate' => $estimate,
            'form' => $form->createView()
        ));
    }

}
