<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Subscriber;
use Symfony\Component\HttpFoundation\Response;

/**
 * Subscriber controller.
 *
 * @Route("/subscribe")
 */
class SubscriberController extends Controller
{
    /**
     * Creates a new Subscriber entity.
     *
     * @Route("/", name="subscribe_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
    	$subscriber = new Subscriber();
    	$em = $this->getDoctrine()->getManager();

    	if ($request->isXmlHttpRequest()) {
    		$subscriber->setEmail($request->get('email'));
    		$validator = $this->get('validator');
    		$errors = $validator->validate($subscriber);

    		if (count($errors) > 0) {
    			$errorsString = $errors[0]->getMessage();

    			return new Response($errorsString);
    		} else {
    			$em->persist($subscriber);
    			$em->flush();

    			return new Response("success");
    		}
    	}

        $form = $this->createForm('AppBundle\Form\SubscriberType', $subscriber);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($subscriber);
            $em->flush();

            return $this->redirectToRoute('homepage');
        }

        return $this->render('subscriber/new.html.twig', array(
            'subscriber' => $subscriber,
            'form' => $form->createView(),
        ));
    }
}
