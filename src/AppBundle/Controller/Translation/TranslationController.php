<?php

namespace AppBundle\Controller\Translation;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class TranslationController extends Controller
{
    /**
     * @Route("/", name="trans_dashboard")
     */
    public function dashboardAction()
    {
    	$repo = $this->getDoctrine()->getManager()->getRepository("AppBundle:ProjectLog");
    	$translations = $repo->findByUser($this->getUser());
    	$translationsCount = $repo->countTranslationsGroupByLocale($this->getUser());
    	$count = 0;
    	$locales_labels = $this->getParameter('locales_labels');
    	$countSize = sizeof($translationsCount);
    	for ($i=0; $i<$countSize; $i++){;
    		$translationsCount[$i]['label'] = $locales_labels[$translationsCount[$i]['label']];
    		$count += $translationsCount[$i]['value'];
    	}

        return $this->render('trans/dashboard.html.twig', array(
        	'nbLanguages'	=> sizeof($translationsCount),
        	'nbTranslations'=> $count,
        	'translations' => $translations,
        	'json' => preg_replace('/"([^"]+)"\s*:\s*/', '$1:', json_encode($translationsCount, JSON_NUMERIC_CHECK)),
        ));
    }

    /**
     * @Route("/projects", name="trans_projects")
     */
    public function projectsAction()
    {
    	$em = $this->getDoctrine()->getManager();
    	$projects = $em->getRepository("AppBundle:Project")->findBy(['published' => true]);

        return $this->render('trans/projects.html.twig', array(
            'projects' => $projects,
        ));
    }
}
