<?php

namespace AppBundle\Controller\Translation;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Project;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use AppBundle\Entity\ProjectLog;

/**
 * Project controller.
 *
 * @Route("/project")
 */
class ProjectController extends Controller
{

    /**
     * Displays a form to edit an existing Project entity.
     *
     * @Route("/{slug}/translate/{locale}", name="project_translate")
     * @Method({"GET", "POST"})
     */
    public function translateAction(Request $request, Project $project, $locale)
    {
    	$project->setLocale($locale);
    	$em = $this->getDoctrine()->getManager();
		$em->refresh($project);
    	$editForm = $this->createFormBuilder($project)->add('brief', TextType::class)->add('description', CKEditorType::class)->getForm();
    	$oldProject = clone $project;
    	$editForm->handleRequest($request);

    	if ($editForm->isSubmitted() && $editForm->isValid()) {
			$project->setTranslatableLocale($locale);
			$em->persist($project);

			/* If change */
			if ($oldProject->getBrief() != $project->getBrief() || $oldProject->getDescription() != $project->getDescription())
			{
				$log = new ProjectLog();
				$log->setProject($project);
				$log->setLocale($locale);
				$log->setUser($this->getUser());
				/* Brief has changed */
				if($oldProject->getBrief() != $project->getBrief())
					$log->setOldBrief($oldProject->getBrief());
				/* Description has changed */
				if($oldProject->getDescription() != $project->getDescription())
					$log->setOldDescription($oldProject->getDescription());
				$em->persist($log);
			}

			$em->flush();
			$this->addFlash('success', 'Merci de ta participation !');
    		return $this->redirectToRoute('trans_projects');
    	}

    	return $this->render('trans/project_trans.html.twig', array(
    			'locale' => $locale,
    			'project' => $project,
    			'edit_form' => $editForm->createView(),
    	));
    }
}
