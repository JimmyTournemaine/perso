<?php 
namespace AppBundle\Mail;

use Symfony\Component\Validator\Constraints as Assert;

class ContactEmail 
{
	/**
	 * @Assert\NotBlank()
	 */
	private $name;
	
	/**
	 * @Assert\NotBlank()
	 * @Assert\Length(min=5,max=128)
	 */
	private $subject;
	
	/**
	 * @Assert\NotBlank()
	 * @Assert\Email()
	 */
	private $email;
	
	/**
	 * @Assert\NotBlank()
	 * @Assert\Length(min=60)
	 */
	private $content;
	
	public function getName() {
		return $this->name;
	}
	public function setName($name) {
		$this->name = $name;
		return $this;
	}
	public function getSubject() {
		return $this->subject;
	}
	public function setSubject($subject) {
		$this->subject = $subject;
		return $this;
	}
	public function getEmail() {
		return $this->email;
	}
	public function setEmail($email) {
		$this->email = $email;
		return $this;
	}
	public function getContent() {
		return $this->content;
	}
	public function setContent($content) {
		$this->content = $content;
		return $this;
	}
	
	
	
}